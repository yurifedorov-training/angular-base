import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { ModalComponent } from '../modal/modal.component';
import { RefDirective } from '../shared/ref.directive';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  @ViewChild(RefDirective, {static: false}) refDir!: RefDirective

  constructor(
    private router: Router,
    public auth: AuthService,
    private resolver: ComponentFactoryResolver,
    private title: Title,
    private meta: Meta
  ) {
    title.setTitle('Home page')
    meta.addTags([
      {name: 'keywords', content: 'angular,google,homepage'},
      {name: 'description', content: 'This is home page'}
    ])
  }

  goToPostsPage() {
    this.router.navigate(['/posts']).then(() => {});
  }

  showModal() {
    const modalFactory = this.resolver.resolveComponentFactory(ModalComponent)
    this.refDir.containerRef.clear()

    const component = this.refDir.containerRef.createComponent<ModalComponent>(modalFactory.componentType)
    component.instance.title = 'Dynamic title'
    component.instance.close.subscribe(() => {
      this.refDir.containerRef.clear()
    })
  }
}

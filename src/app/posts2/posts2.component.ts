import { Component, OnInit } from '@angular/core';
import { Posts2Service } from './posts2.service';

@Component({
  selector: 'app-posts2',
  template: `Posts2 component`,
})
export class Posts2Component implements OnInit {
  posts = [] as any;
  message!: string

  constructor(
    private service: Posts2Service
  ) { }

  ngOnInit(): void {
    this.service.fetch().subscribe(p => {
      this.posts = p
    })
  }

  add(title: string) {
    const post  = { title }
    this.service.create(post).subscribe(p => {
      this.posts.push(p)
    }, err => this.message = err)
  }

  delete(id: number) {
    if (window.confirm('Are you sure?')) {
      this.service.remove(id).subscribe()
    }
  }

}

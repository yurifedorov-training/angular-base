import { Posts2Component } from './posts2.component';
import { Posts2Service } from './posts2.service';
import { EMPTY, of, throwError } from 'rxjs';
import { ComponentFixture, fakeAsync, tick, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

describe('Posts2Component', () => {
  let fixture: ComponentFixture<Posts2Component>
  let component: Posts2Component;
  let service: Posts2Service

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ Posts2Component ],
      providers: [ Posts2Service ],
      imports: [ HttpClientModule ]
    })

    fixture = TestBed.createComponent(Posts2Component)
    component = fixture.componentInstance
    service = TestBed.inject(Posts2Service)
  });

  // UNIT TESTS
  it('should call fetch then ngOnInit', () => {
    const spy = spyOn(service, 'fetch').and.callFake(() => {
      return EMPTY
    })

    component.ngOnInit()

    expect(spy).toHaveBeenCalled()
  });

  it('should update posts length after ngOnInit', () => {
    const posts = [1, 2, 3, 4]
    spyOn(service, 'fetch').and.returnValue(of(posts))

    component.ngOnInit()

    expect(component.posts.length).toBe(posts.length)
  });

  it('should add new post', function() {
    const post = {title: 'test'}
    const spy = spyOn(service, 'create').and.returnValue(of(post))
    component.add(post.title)
    expect(spy).toHaveBeenCalled()
    expect(component.posts.includes(post)).toBeTruthy()
  });

  it('should set message to error message', function() {
    const err = 'Error message'
    spyOn(service, 'create').and.returnValue(throwError(() => err))
    component.add('Post title')
    expect(component.message).toBe(err)
  });

  it('should remove post if user confirms', function() {
    const spy = spyOn(service, 'remove').and.returnValue(EMPTY)
    spyOn(window, 'confirm').and.returnValue(true)
    component.delete(10)
    expect(spy).toHaveBeenCalledWith(10)
  });

  it('should NOT remove post if user doesnt confirm', function() {
    const spy = spyOn(service, 'remove').and.returnValue(EMPTY)
    spyOn(window, 'confirm').and.returnValue(false)
    component.delete(10)
    expect(spy).not.toHaveBeenCalled()
  });

  // INTEGRATIONS TESTS
  it('should fetch posts on ngOnInit', function() {
    const posts = [1, 2, 3]
    spyOn(service, 'fetch').and.returnValue(of(posts))
    fixture.detectChanges()
    expect(component.posts).toEqual(posts)
  });

  // ERROR!!!!
  // xit('should fetch posts on ngOnInit (promise)', fakeAsync(() => {
  //   const posts = [1, 2, 3]
  //   spyOn(service, 'fetchPromise').and.returnValue(Promise.resolve(posts))
  //   fixture.detectChanges()
  //   tick()
  //   expect(component.posts.length).toBe(posts.length)
  // }))
});

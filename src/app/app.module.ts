import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppComponent } from './app.component'
import { FormsModule } from '@angular/forms'
import { HomeComponent } from './home/home.component'
import { AppRoutingModule } from './app-routing.module'
import { ErrorPageComponent } from './error-page/error-page.component'
import { SharedModule } from './shared/shared.module';
import { ModalComponent } from './modal/modal.component';
import { RefDirective } from './shared/ref.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { CounterComponent } from './counter/counter.component';
import { Posts2Component } from './posts2/posts2.component';
import { RoutingComponent } from './routing/routing.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorPageComponent,
    ModalComponent,
    RefDirective,
    CounterComponent,
    Posts2Component,
    RoutingComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [],
  entryComponents: [ModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}

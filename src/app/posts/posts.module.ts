import { NgModule } from '@angular/core';
import { PostsComponent } from './posts.component';
import { PostComponent } from './post/post.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { PostResolver } from '../shared/post.resolver';

@NgModule({
  declarations: [
    PostsComponent,
    PostComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {path: 'posts', component: PostsComponent, canActivate: [AuthGuard]},
      {
        path: 'posts/:id',
        component: PostComponent,
        resolve: {
          post: PostResolver
        }
      },
    ])
  ]
})
export class PostsModule {}

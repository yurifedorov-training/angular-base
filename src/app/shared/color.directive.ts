import {Directive, HostBinding, OnInit, Input, ElementRef, OnChanges} from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective implements OnInit, OnChanges {
  @HostBinding('style.color') color!: string

  defaultColor = 'blue'
  @Input('appColor') bgColor!: string

  constructor(
    private el: ElementRef
  ) {
  }

  ngOnInit() {
    this.color = '#aaa'
  }

  ngOnChanges(): void {
    this.el.nativeElement.style.backgroundColor = this.bgColor || this.defaultColor
  }
}

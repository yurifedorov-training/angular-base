import { CounterComponent } from './counter.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('CounterComponent', () => {
  let component: CounterComponent
  let fixture: ComponentFixture<CounterComponent>

  /**
   * beforeEach() - Выполнить перед КАЖДЫМ тестом
   * beforeAll() - Выполнить один раз перед ВСЕМИ тестами
   * afterEach() - Выполнить после каждого теста
   * afterAll() - Выполнить один раз после ВСЕХ тестов
   * */

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterComponent ],
      imports: [ ReactiveFormsModule ]
    })

    fixture = TestBed.createComponent(CounterComponent)
    component = fixture.componentInstance
    // fixture.debugElement
    // fixture.nativeElement
  })

  // UNIT TESTS
  it('should increment counter by 1', function() {
    component.increment()
    expect(component.counter).toBe(1)
  });

  it('should decrement counter by 1', function() {
    component.decrement()
    expect(component.counter).toBe(-1)
  })

  it('should increment value by event emitter', function() {
    let result = 0
    component.counterEmitter.subscribe(v => result = v)
    component.increment()
    expect(result).toBe(1)
  })

  it('should create form with 2 controls', function() {
    expect(component.form.contains('login')).toBeTruthy()
    expect(component.form.contains('email')).toBeTruthy()
  });

  it('should mark login as invalid if empty value', function() {
    const control = component.form.get('login')
    control?.setValue('')
    expect(control?.valid).toBeFalsy()
  });

  // INTEGRATIONS TESTS
  it('should be created', function() {
    expect(component).toBeDefined()
  });

  it('should render counter property', function() {
    let num = 42
    component.counter = num

    fixture.detectChanges()

    let de = fixture.debugElement.query(By.css('.counter'))
    let el: HTMLElement = de.nativeElement

    expect(el.textContent).toContain(num.toString())
  });

  it('should add green class if counter is even', function() {
    component.counter = 6
    fixture.detectChanges()

    let de = fixture.debugElement.query(By.css('.counter'))
    let el: HTMLElement = de.nativeElement

    expect(el.classList.contains('green')).toBeTruthy()
  });

  it('should increment counter if increment button was clicked', function() {
    let btn = fixture.debugElement.query(By.css('#increment'))
    btn.triggerEventHandler('click', null)

    expect(component.counter).toBe(1)
  });
});

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AuthService {
  public isAuth = false

  login() {
    this.isAuth = true
    alert('Вы авторизованы!')
  }

  logout() {
    this.isAuth = false
    alert('Вы завершили сессию')
  }

  isLoggedIn(): Observable<boolean> {
    if (this.isAuth) {
      return of(this.isAuth = true).pipe()
    } else {
      return of(this.isAuth = false).pipe()
    }

  }
}
